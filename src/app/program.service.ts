import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Program} from './program';
import {Activity} from './activity';
import {ActivityInput} from './activity-input';


const httpOptions = {
  headers: new HttpHeaders({
    'Authorization': 'Bearer GfR6vIHG0zTWaJle6TjNXvYUrjDn6g',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  private programsUrl = 'https://dev.toladata.io/api/workflowlevel1/';
  private activitiesUrl = 'https://dev.toladata.io/api/workflowlevel2/';

  constructor(
    private http: HttpClient
  ) {
  }

  getPrograms(): Observable<Program[]> {
    return this.http.get<Program[]>(this.programsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getPrograms', []))
      );
  }

  getActivies(id: number): Observable<Activity[]> {
    const url = `${this.activitiesUrl}?workflowlevel1__id=${id}`;
    return this.http.get<Activity[]>(url, httpOptions).pipe(
      catchError(this.handleError<Activity[]>(`getActivities id=${id}`))
    );
  }

  addActivity(id: number, activity: ActivityInput): Observable<Activity> {
    const url = `${this.activitiesUrl}?workflowlevel1__id=${id}`;
    return this.http.post<Activity>(url, activity, httpOptions).pipe(
      catchError(this.handleError<Activity>(`getActivities id=${id}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
