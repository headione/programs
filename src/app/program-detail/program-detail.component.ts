import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

import {ProgramService} from '../program.service';
import {Activity} from '../activity';

@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.css']
})
export class ProgramDetailComponent implements OnInit {
  @Input() activities: Activity[];
  @Input() id: number;

  constructor(
    private route: ActivatedRoute,
    private programService: ProgramService,
    private location: Location
  ) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.getActivities();
    this.id = id;
  }

  getActivities(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.programService.getActivies(id)
      .subscribe(activities => this.activities = activities);
  }

  goBack(): void {
    this.location.back();
  }

  delete(activity: Activity): void {
    this.activities = this.activities.filter(a => a !== activity);
    // TODO: delete activity
    // this.programService.deleteActivity(activity).subscribe();
  }
}
