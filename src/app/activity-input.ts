export class ActivityInput {
  constructor(
    public name: string,
    public startdate?: string,
    public enddate?: string,
  ) {
  }
}
