import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProgramsComponent } from './programs/programs.component';
import { ProgramDetailComponent } from './program-detail/program-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/programs', pathMatch: 'full' },
  { path: 'programs/:id', component: ProgramDetailComponent },
  { path: 'programs', component: ProgramsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
