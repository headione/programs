import {Component, OnInit, Input} from '@angular/core';
import {ActivityInput} from '../activity-input';
import {ProgramService} from '../program.service';

@Component({
  selector: 'app-activity-form',
  templateUrl: './activity-form.component.html',
  styleUrls: ['./activity-form.component.css']
})
export class ActivityFormComponent implements OnInit {
  @Input() id: number;

  constructor(
    private programService: ProgramService,
  ) {
  }

  model = new ActivityInput('', '', '');
  submitted = false;

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit(): void {
    this.submitted = true;
    // TODO: add activity
    // this.programService.addActivity(this.id, this.model);
  }
}
